// TDD . test driven developement - unit testing

const expect = require('chai').expect;
const converter = require('../src/converter');


describe("Color code converter", () => {
    describe("RGB to Hex conversion", () => {
        it("converts the basic from rgb to hex colors", () => {
            const redHex = converter.rgbToHex(255, 0, 0); // ff0000
            const greenHex = converter.rgbToHex(0, 255, 0); // 00ff00
            const blueHex = converter.rgbToHex(0, 0, 255); // 0000ff

            expect(redHex).to.equal("ff0000")
            expect(greenHex).to.equal("00ff00")
            expect(blueHex).to.equal("0000ff")
        })
    }),
    // describe hex to rgb
    describe("Hex to RGB conversion", () => {
        it("converts the basic from hex to rgb colors", () => {
            const redRgb = converter.hexToRgb("ff0000"); // ff0000
            const greenRgb = converter.hexToRgb("00ff00"); // 00ff00
            const blueRgb = converter.hexToRgb("0000ff"); // 0000ff

            expect(redRgb).to.equal("rgb(255,0,0)")
            expect(greenRgb).to.equal("rgb(0,255,0)")
            expect(blueRgb).to.equal("rgb(0,0,255)")
        })
    })
})