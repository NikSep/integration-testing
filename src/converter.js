const pad = (hex) => {
    return (hex.length === 1 ? "0"+hex : hex);
}

module.exports = {
    rgbToHex: (red,green,blue) => {
        const redHex = red.toString(16); //"02"
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);

        return pad(redHex) + pad(greenHex) + pad(blueHex);
    }// ,
    // hexToRgb
    ,
    hexToRgb: (colour) => {
        var r,g,b;
        if ( colour.charAt(0) == '#' ) {
            colour = colour.substr(1);
        }
        if ( colour.length == 3 ) {
            colour = colour.substr(0,1) + colour.substr(0,1) + colour.substr(1,2) + colour.substr(1,2) + colour.substr(2,3) + colour.substr(2,3);
        }
        r = colour.charAt(0) + '' + colour.charAt(1);
        g = colour.charAt(2) + '' + colour.charAt(3);
        b = colour.charAt(4) + '' + colour.charAt(5);
        r = parseInt( r,16 );
        g = parseInt( g,16 );
        b = parseInt( b ,16);
        return 'rgb(' + r + ',' + g + ',' + b + ')';}
}